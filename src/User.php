<?php

namespace App;
use IteratorAggregate;
use Ratchet\ConnectionInterface;

class User implements IteratorAggregate {

    public $id;

    /**
     * @var Task[]
     */
    protected $tasks;

    public function __construct($id) {
        $this->tasks = [];
        $this->id = $id;
    }

    public function addTask($taskId, ConnectionInterface $conn) {
        if (!$this->tasks[$taskId]) {
            $this->tasks[$taskId] = new Task($taskId);
        }
        $this->tasks[$taskId]->addConnection($conn);
    }

    public function getIterator() {
        return new Iterator($this->tasks);
    }

    public function hasTasks() {
        return !empty($this->tasks);
    }

    public function close(ConnectionInterface $conn) {
        foreach ($this->tasks as $task) {
            if ($task->close($conn)) {
                if (!$task->hasConnections()) {
                    unset($this->tasks[$task->id]);
                }
                return true;
            }
        }
        return false;
    }

    public function getTasks() {
        return array_keys($this->tasks);
    }
}