<?php

namespace App;

class Observer {

    private static $inst = null;

    private $observers = [];

    final private function __construct() { }

    private static function getInstance() {
        if(is_null(self::$inst)) {
            self::$inst = new self();
        }
        return self::$inst;
    }

    public static function addObserver($command, IEventHandler $observer) {
        $instance = self::getInstance();
        if(isset($instance->observers[$command])) {
            $instance->observers[$command] = [];
        }
        $instance->observers[$command][] = $observer;
    }

    public static function removeObserver($command, IEventHandler $observer) {
        $instance = self::getInstance();
        if(isset($instance->observers[$command])) {
            $data = [];
            foreach($instance->observers[$command] as $obs) {
                if($observer == $obs) {
                    continue;
                }
                $data[] = $obs;
            }
            $instance->observers[$command] = $data;
        }
    }

    public static function notify(Event $event) {
        $instance = self::getInstance();
        $command = $event->getCommand();
        if(isset($instance->observers[$command])) {
            foreach($instance->observers[$command] as $obs) {
                $obs->handler($event);
            }
        }
    }
}