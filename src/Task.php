<?php

namespace App;
use IteratorAggregate;
use Ratchet\ConnectionInterface;

class Task implements IteratorAggregate {

    public $id;
    /**
     * @var ConnectionInterface[]
     */
    protected $connections;

    public function __construct($id) {
        $this->connections = [];
        $this->id = $id;
    }

    public function addConnection(ConnectionInterface $conn) {
        if (!$this->connections[$conn->resourceId]) {
            $this->connections[$conn->resourceId] = $conn;
        }
    }

    public function getIterator() {
        return new Iterator($this->connections);
    }

    public function hasConnections() {
        return !empty($this->connections);
    }

    public function close(ConnectionInterface $conn) {
        if ($this->connections[$conn->resourceId]) {
            unset($this->connections[$conn->resourceId]);
            return true;
        }
        return false;
    }
}