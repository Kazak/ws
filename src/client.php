<?php

require dirname(__DIR__) . '/vendor/autoload.php';


list($command, $params) = parse_arguments($argv);

\Ratchet\Client\connect('ws://127.0.0.1:8001')->then(function($conn) use ($command, $params) {
    $conn->on('message', function($msg) use ($conn, $command, $params) {
        $data = json_decode($msg, true);
        if ($data[0] == 'user-list') {
            echo "User list:\n";
            echo implode("\n", $data[1]['users']);
            echo "\n";
        } elseif ($data[0] == 'task-list') {
            echo "Tasks list:\n";
            echo implode("\n", $data[1]['tasks']);
            echo "\n";
        }
        $conn->close();
    });

    $conn->send(json_encode([$command, $params]));
}, function ($e) {
    echo "Could not connect: {$e->getMessage()}\n";
    exit(1);
});


function parse_arguments($argv) {
    unset($argv[0]);

    $knownCommands = [
        'get-all-users',
        'get-all-user-task',
        'send-message'
    ];

    $knownParams = [
        'task',
        'message'
    ];

    $command = $user = null;
    $params = [];

    foreach ($argv as $arg) {
        list($param, $value) = explode('=', $arg);
        if (in_array($param, $knownCommands)) {
            $command = $param;
            $user = $value;
        } elseif (in_array($param, $knownParams)) {
            $params[$param] = $value;
        }
    }

    if (!$command) {
        usage();
    }

    if ($command == 'get-all-users') {
        $params = [];
    } else {
        if ($command == 'get-all-user-task') {
            if (is_numeric($user)) {
                $params = ['user' => $user];
            }
        } else {
            $params['user'] = $user;
            if (!$params['message']) {
                usage();
            }
        }
        if (!$params['user']) {
            usage();
        }
    }

    return [$command, $params];
}

function usage() {
    echo "Command not found\n";
    echo "Usage:\tget-all-users\t\t\t\t\t-- Show all connected users\n";
    echo "\tget-all-user-task=userId\t\t\t-- Show all user's tasks\n";
    echo "\tsend-message=all message=\"text\"\t\t\t-- Send massage to all users\n";
    echo "\tsend-message=userId message=\"text\"\t\t-- Send message to all user's tasks\n";
    echo "\tsend-message=userId task=taskId message=\"text\"\t-- Send message to task\n";
    exit(1);
}