<?php

namespace App;
use Iterator as Iterate;

class Iterator implements Iterate
{
    private $items = [];

    public function __construct($items)
    {
        if (is_array($items)) {
            $this->items = $items;
        }
    }

    public function rewind()
    {
        reset($this->items);
    }

    public function current()
    {
        return current($this->items);
    }

    public function key()
    {
        return key($this->items);
    }

    public function next()
    {
        return next($this->items);
    }

    public function valid()
    {
        $key = key($this->items);
        return ($key !== null && $key !== false);
    }
}