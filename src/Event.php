<?php

namespace App;
use Exception;
use Ratchet\ConnectionInterface;

class Event {

    const GET_ALL_USERS = 'get-all-users';

    const GET_ALL_USER_TASK = 'get-all-user-task';

    const SEND_MESSAGE = 'send-message';

    const COMMANDS = [
        self::GET_ALL_USERS,
        self::GET_ALL_USER_TASK,
        self::SEND_MESSAGE
    ];

    private $command;

    private $params;

    private $connection;

    public function __construct($command, array $params, ConnectionInterface $connection) {
        if (!in_array($command, self::COMMANDS)) {
            throw new Exception('unknown type of event');
        }
        $this->command = $command;
        $this->params = $params;
        $this->connection = $connection;
    }

    public function getCommand() {
        return $this->command;
    }

    public function getParams() {
        return $this->params;
    }

    public function getConnection() {
        return $this->connection;
    }
}