<?php

namespace App;

interface IEventHandler {
    public function handler(Event $event);
}