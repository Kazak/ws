<?php
/**
 * Created by PhpStorm.
 * User: Anton Byvshev
 * Date: 05.11.16
 * Time: 16:30
 */

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use App\TaskHandler;

require dirname(__DIR__) . '/vendor/autoload.php';

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new TaskHandler()
        )
    ),
    8001
);

$server->run();