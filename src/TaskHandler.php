<?php

namespace App;
use Exception;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class TaskHandler implements MessageComponentInterface {

    protected $userService;

    public function __construct() {
        $this->userService = new UserService();
    }

    public function onOpen(ConnectionInterface $conn) {
        $this->userService->onOpen($conn);
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        list($command, $params) = json_decode($msg, true);

        try {
            $event = new Event($command, (array)$params, $from);
            Observer::notify($event);
        } catch (Exception $e) {

        }
    }

    public function onClose(ConnectionInterface $conn) {
        $this->userService->onClose($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
}