<?php

namespace App;
use Ratchet\ConnectionInterface;

class UserService implements IEventHandler {

    /**
     * @var User[]
     */
    protected $users;

    protected $connections;

    public function __construct() {
        $this->users = [];
        $this->connections = [];

        Observer::addObserver(Event::GET_ALL_USERS, $this);
        Observer::addObserver(Event::GET_ALL_USER_TASK, $this);
        Observer::addObserver(Event::SEND_MESSAGE, $this);
    }

    public function onOpen(ConnectionInterface $conn) {
        $query = $conn->WebSocket->request->getQuery();
        $userId = $query->get('user_id');
        $taskId = $query->get('task_id');
        if ($userId && $taskId) {
            if (!$this->users[$userId]) {
                $this->users[$userId] = new User($userId);
            }
            $this->users[$userId]->addTask($taskId, $conn);
            $this->connections[$conn->resourceId] = $userId;
        }
    }

    public function onClose(ConnectionInterface $conn) {
        if ($userId = $this->connections[$conn->resourceId]) {
            if ($this->users[$userId]->close($conn)) {
                if (!$this->users[$userId]->hasTasks()) {
                    unset($this->users[$userId]);
                }
            }
            unset($this->connections[$conn->resourceId]);
            echo "Connection {$conn->resourceId} has disconnected\n";
        }
    }

    public function handler(Event $event) {
        switch ($event->getCommand()) {
            case 'get-all-users':
                $users = $this->getAllUsers();
                $event->getConnection()->send(json_encode(['user-list', ['users' => $users]]));
                break;
            case 'get-all-user-task':
                $tasks = $this->getAllUserTask($event->getParams()['user']);
                $event->getConnection()->send(json_encode(['task-list', ['tasks' => $tasks]]));
                break;
            case 'send-message':
                $this->sendMessage($event->getParams());
                $event->getConnection()->send(json_encode(['send-message', 'success']));
                break;
        }
    }

    private function getAllUsers() {
        return array_keys($this->users);
    }

    private function getAllUserTask($userId) {
        return $this->users[$userId]->getTasks();
    }

    private function sendMessage($params) {
        if ($params['user'] == 'all') {
            foreach ($this->users as $user) {
                foreach ($user as $task) {
                    foreach ($task as $connection) {
                        /**
                         * @var ConnectionInterface $connection
                         */
                        $connection->send(json_encode(['message', $params['message']]));
                    }
                }
            }
        } else {
            foreach ($this->users[$params['user']] as $task) {
                /**
                 * @var Task $task
                 */
                if ($params['task'] && $task->id !== $params['task']) {
                    continue;
                }
                foreach ($task as $connection) {
                    /**
                     * @var ConnectionInterface $connection
                     */
                    $connection->send(json_encode(['message', $params['message']]));
                }
            }
        }
    }
}